# Multiselect List

This Angular component is designed to universally handle arrays containing any object type, dynamically rendering a list in which each item is paired with a checkbox. It allows the host to define display templates for both headers and items, ensuring customizability. The component supports enabling or disabling checkboxes based on specific conditions and includes a 'select-all' checkbox and a download button for efficient bulk operations.

<img src="./src/assets/readme-screen-recording.gif" alt="Screen recording" style="margin: 20px 0; width: 700"/>

## Set up

### Prerequisites

To ensure full compatibility and functionality, please use the following:

- **Node.js**: Version 18.20.2
- **npm**: Version 10.5.0
- **Angular CLI**: Version 16.2.14

These specific versions are required to match the project's configuration and dependency requirements -- using different versions might lead to compatibility issues and unexpected behavior.

### Installation

This project is set up using Angular and can be run locally the following commands:

- **Install Dependencies**

  Installs all the necessary dependencies required by the project.

  `npm i`

- **Serve the Application**

  Compile the application and serve it on http://localhost:4200/.

  `ng serve`

- **Run Tests**

  Executes the unit tests.

  `ng test`

- **Additional Commands**:
  - `ng lint` to lint the project files
  - `ng prettify` to fix formatting issues
  - `ng test --code-coverage` for a code coverage report

## Built with

- **Angular 16/TypeScript/SCSS**: for primary development
- **Karma/Jasmine**: for running tests
- **RxJS**: for reactive programming in Angular
- **ESLint and Prettier**: to ensure code quality and consistent formatting
- **Husky**: to manage pre-commit hooks

## About

### Assumptions Made

- The component is designed for general applicability to different lists with unknown columns, display requirements and data structures.
- Users of this application may access it on mobile devices.
- Data will eventually be fetched from a server. It will not contain duplicates, might be empty, or could consist entirely of unavailable items.
- `data-refs` will be used for DOM manipulation in spec tests so that CSS selectors can change without affecting tests.

### UX changes compared to the spec screenshot

- **Cursor: pointer**

  I have removed the `cursor:pointer` style and its implied functionality that would allows users to click anywhere on the list item to toggle its checkbox. Implementing this feature in an accessible manner— i.e. by grouping the entire list item into a single focusable element—would restrict our ability to introduce item-specific actions (such as edit or delete) in future updates. This change ensures that each clickable element can be individually tabbed to and interacted with via keyboard, enhancing accessibility while maintaining the flexibility to add more complex functionalities moving forward.

- **Small screens**

  To avoid cutting off too much vital information/horizontal scrolling, I implemented a more mobile-friendly version of the list that appears in narrower screens.

<img src="./src/assets/readme-mobile-screenshot.png" alt="Mobile screenshot" style="width: 300; margin: 20px 40px"/>

## Future Enhancements

If more time were available, I would focus on:

- **Creating a reusable checkbox component**

  To achieve uniform appearance across various browsers and refine visual presentation.

- **Enhancing user guidance**

  Implement better mechanisms to inform users about errors or restrictions when they attempt to interact with disabled items. Provide stronger visual indicators of when items aren't clickable.

- **Implementing a robust unique ID generator**

  The current implementation of using unique IDs to connect ARIA labels and inputs poses two main restrictions - (1) only one Multiselect List is allowed per page and (2) `uniqueItemIdKey` values must include only characters valid in HTML IDs. If we were to expand and handle unpredictable data at scale, a more reliable ID generation process would be necessary.

## License

The MIT License (MIT)

Copyright (c) 2024 Olivia Madrid

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
