module.exports = {
  '*.ts': ['eslint --fix', 'prettier --write'],
  '*.scss': ['prettier --write'],
  '*.html': ['prettier --write'],
};
