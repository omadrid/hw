import { TestBed, fakeAsync, flush, tick } from '@angular/core/testing';
import { MultiSelectListService } from './multi-select-list.service';
import { Subscription } from 'rxjs';
import { MultiSelectListComponent } from '../multi-select-list.component';
import { CheckboxState } from '../../../shared/models/types/checkbox-state.type';

describe('MultiSelectListService', () => {
  let service: MultiSelectListService<any>;
  let subs: Subscription[] = [];

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MultiSelectListComponent],
      providers: [MultiSelectListService],
    });
    service = TestBed.inject(MultiSelectListService);
    service.itemDisabledFn = (item) => item?.disabled === true;
  });

  afterEach(() => {
    subs.forEach((sub) => sub.unsubscribe());
    subs = [];
  });

  describe('initialization', () => {
    it('should create', () => {
      expect(service).toBeTruthy();
    });

    it('should emit an empty array from items$ on initialization', (done: DoneFn) => {
      subs.push(
        service.items$.subscribe((items) => {
          expect(items).toEqual([]);
          done();
        })
      );
    });

    it('should emit an empty set from selectedItems$ on initialization', (done: DoneFn) => {
      subs.push(
        service.selectedItems$.subscribe((selectedItems) => {
          expect(selectedItems.size).toBe(0);
          expect(Array.from(selectedItems)).toEqual([]);
          done();
        })
      );
    });

    it(`should emit 'unchecked' from selectAllCheckboxState$ on initialization`, (done: DoneFn) => {
      subs.push(
        service.selectAllCheckboxState$.subscribe((state) => {
          expect(state.value).toBe('unchecked');
          done();
        })
      );
    });
  });

  describe('setting items', () => {
    it('should emit the updated item list correctly after setting items', fakeAsync(() => {
      const items = [{ id: 1 }, { id: 2 }];
      let itemsFromObservable: any[] = [];

      subs.push(
        service.items$.subscribe((items) => {
          itemsFromObservable = items;
        })
      );

      service.setItems(items);
      tick();

      expect(itemsFromObservable.length).toEqual(2);
      expect(itemsFromObservable).toEqual(items);
      flush();
    }));

    it('should clear selectedItems when refreshed with an empty array of items ', fakeAsync(() => {
      (service as any).selectedItemsSource.next(new Set([{ id: 1 }]));
      tick();

      service.setItems([]);
      tick();

      let selectedItemsFromObservable = new Set();
      subs.push(
        service.selectedItems$.subscribe((selectedItems) => {
          selectedItemsFromObservable = selectedItems;
        })
      );
      expect(selectedItemsFromObservable.size).toBe(0);

      flush();
    }));
  });

  describe('updating selected items', () => {
    it('should update selectedItems$ correctly', fakeAsync(() => {
      const item = { id: 1 };
      let selectedItemsFromObservable = new Set();

      subs.push(
        service.selectedItems$.subscribe((selectedItems) => {
          selectedItemsFromObservable = selectedItems;
        })
      );

      service.toggleItemSelection(item);
      tick();

      expect(selectedItemsFromObservable.has(item)).toBeTrue();
      expect(selectedItemsFromObservable.size).toBe(1);
      flush();
    }));

    it('should add an item to the selected set if it is not already selected', (done: DoneFn) => {
      const testItem = { id: 1, name: 'Item 1' };
      service.setItems([testItem]);
      service.toggleItemSelection(testItem);
      subs.push(
        service.selectedItems$.subscribe((selectedItems) => {
          expect(selectedItems.has(testItem)).toBeTrue();
          done();
        })
      );
    });

    it('should remove an item from the selected set if it is already selected', (done: DoneFn) => {
      const testItem = { id: 1, name: 'Item 1' };
      service.setItems([testItem]); // Make sure the item is available in items$
      (service as any).selectedItemsSource.next(new Set([testItem]));

      service.toggleItemSelection(testItem);

      subs.push(
        service.selectedItems$.subscribe((selectedItems) => {
          expect(selectedItems.has(testItem)).toBeFalse();
          done();
        })
      );
    });

    describe('updating selected items', () => {
      it('should not add a disabled item to the selectedItems set', (done: DoneFn) => {
        const testItem = { id: 1, name: 'Item 1', disabled: true };
        service.setItems([testItem]);

        service.toggleItemSelection(testItem);

        subs.push(
          service.selectedItems$.subscribe((selectedItems) => {
            expect(selectedItems.has(testItem)).toBeFalse();
            done();
          })
        );
      });
    });
  });

  describe('selecting and unselecting multiple items', () => {
    it('should select all enabled items', fakeAsync(() => {
      const items = [
        { id: 1, name: 'Item 1', disabled: false },
        { id: 2, name: 'Item 2', disabled: true },
        { id: 3, name: 'Item 3', disabled: false },
      ];
      service.setItems(items);
      tick();
      service.selectAllItems();
      tick();

      subs.push(
        service.selectedItems$.subscribe((selectedItems) => {
          expect(selectedItems.size).toBe(2);
          expect(selectedItems.has(items[0])).toBeTrue();
          expect(selectedItems.has(items[2])).toBeTrue();
        })
      );
      flush();
    }));

    it('should clear all selections with unselectAllItems', fakeAsync(() => {
      const items = [
        { id: 1, name: 'Item 1', disabled: false },
        { id: 2, name: 'Item 2', disabled: false },
      ];
      service.setItems(items);
      service.selectAllItems();
      tick();

      service.unselectAllItems();
      tick();

      subs.push(
        service.selectedItems$.subscribe((selectedItems) => {
          expect(selectedItems.size).toBe(0);
        })
      );
      flush();
    }));

    it('should select all enabled items correctly', fakeAsync(() => {
      const items = [
        { id: 1, name: 'Item 1', disabled: false },
        { id: 2, name: 'Item 2', disabled: false },
      ];
      service.setItems(items);
      tick();

      service.toggleSelectAll();
      tick();

      let selectedItems = new Set();
      subs.push(
        service.selectedItems$.subscribe((items) => {
          selectedItems = items;
        })
      );
      tick();

      expect(selectedItems.size).toBe(2);
      expect(selectedItems.has(items[0])).toBeTrue();
      expect(selectedItems.has(items[1])).toBeTrue();

      flush();
    }));

    it('should unselect all items correctly', fakeAsync(() => {
      const items = [
        { id: 1, name: 'Item 1', disabled: false },
        { id: 2, name: 'Item 2', disabled: false },
      ];
      service.setItems(items);
      service.toggleSelectAll(); // Select all items first
      tick();

      service.toggleSelectAll(); // Now unselect all
      tick();

      let selectedItems = new Set();
      subs.push(
        service.selectedItems$.subscribe((items) => {
          selectedItems = items;
        })
      );
      tick();
      expect(selectedItems.size).toBe(0);

      flush();
    }));

    it('should correctly update checkbox state value with combineLatest when items or selections change', fakeAsync(() => {
      const items = [
        { id: 1, name: 'Item 1', disabled: false },
        { id: 2, name: 'Item 2', disabled: false },
        { id: 3, name: 'Item 3', disabled: true },
      ];
      service.setItems(items);
      let state: CheckboxState;

      subs.push(
        service.selectAllCheckboxState$.subscribe((checkboxState) => {
          state = checkboxState;
        })
      );

      // No items selected
      tick();
      expect(state.value).toBe('unchecked');

      // Select one item
      service.toggleItemSelection(items[0]);
      tick();
      expect(state.value).toBe('indeterminate');

      // Select all enabled items
      service.selectAllItems();
      tick();
      expect(state.value).toBe('checked');

      // Unselect one item
      service.toggleItemSelection(items[0]);
      tick();
      expect(state.value).toBe('indeterminate');

      // Clear all selections
      service.unselectAllItems();
      tick();
      expect(state.value).toBe('unchecked');
      flush();
    }));

    it('should disable the selectAll checkbox when no enabled items exist', fakeAsync(() => {
      const items = [
        { id: 1, name: 'Item 1', disabled: true },
        { id: 2, name: 'Item 2', disabled: true },
        { id: 3, name: 'Item 3', disabled: true },
      ];
      service.setItems(items);
      let state: CheckboxState;

      subs.push(
        service.selectAllCheckboxState$.subscribe((checkboxState) => {
          state = checkboxState;
        })
      );

      tick();
      expect(state.disabled).toBeTrue();

      flush();
    }));

    it('should not disable the selectAll checkbox when enabled items exist', fakeAsync(() => {
      const items = [
        { id: 1, name: 'Item 1', disabled: false },
        { id: 2, name: 'Item 2', disabled: true },
        { id: 3, name: 'Item 3', disabled: true },
      ];
      service.setItems(items);
      let state: CheckboxState;

      subs.push(
        service.selectAllCheckboxState$.subscribe((checkboxState) => {
          state = checkboxState;
        })
      );

      tick();
      expect(state.disabled).toBeFalse();

      flush();
    }));
  });

  describe('simulated component interaction', () => {
    it('should verify data is propagated correctly', fakeAsync(() => {
      const items = [
        { id: 1, name: 'Item 1', disabled: false },
        { id: 2, name: 'Item 2', disabled: true },
      ];
      service.setItems(items);
      tick();

      let allItems = [],
        selectedItems = new Set(),
        checkboxState: CheckboxState;
      subs.push(
        service.items$.subscribe((data) => (allItems = data)),
        service.selectedItems$.subscribe((data) => (selectedItems = data)),
        service.selectAllCheckboxState$.subscribe(
          (state) => (checkboxState = state)
        )
      );

      tick();

      expect(allItems.length).toBe(2);
      expect(selectedItems.size).toBe(0);
      expect(checkboxState.value).toBe('unchecked');

      service.selectAllItems();
      tick();

      expect(selectedItems.size).toBe(1); // Only one should be selectable
      expect(checkboxState.value).toBe('checked'); // Because all enabled items are selected

      service.toggleItemSelection(items[0]); // Toggle the one selectable item off
      tick();

      expect(selectedItems.has(items[0])).toBeFalse();
      expect(checkboxState.value).toBe('unchecked');

      flush();
    }));
  });
});
