import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, combineLatest, first, map } from 'rxjs';
import { CheckboxState } from '../../../shared/models/types/checkbox-state.type';

@Injectable()
export class MultiSelectListService<T> {
  private itemsSource = new BehaviorSubject<T[]>([]);
  private selectedItemsSource = new BehaviorSubject<Set<T>>(new Set());

  items$ = this.itemsSource.asObservable();
  selectedItems$ = this.selectedItemsSource.asObservable();

  selectedItemsCount$: Observable<number> = this.selectedItems$.pipe(
    map((items) => items.size)
  );

  /** Function to determine if an item is disabled */
  itemDisabledFn: (item: T) => boolean = () => false;

  /** All items that are not disabled */
  enabledItems$: Observable<T[]> = this.items$.pipe(
    map((items) => items.filter((item) => !this.itemDisabledFn(item)))
  );

  enabledItemsCount$: Observable<number> = this.enabledItems$.pipe(
    map((items) => items.length)
  );

  selectAllCheckboxState$: Observable<CheckboxState> = combineLatest([
    this.enabledItems$,
    this.selectedItems$,
  ]).pipe(
    map(([enabledItems, selectedItems]) => {
      const allSelected =
        selectedItems.size === enabledItems.length && selectedItems.size > 0;
      const noneSelected = selectedItems.size === 0;
      return {
        value: noneSelected
          ? 'unchecked'
          : allSelected
            ? 'checked'
            : 'indeterminate',
        disabled: enabledItems.length === 0,
      };
    })
  );

  setItems(items: T[]): void {
    this.itemsSource.next(items);
    if (items.length === 0) {
      // Reset selectedItems if items is refreshed with an empty list
      this.selectedItemsSource.next(new Set());
    }
  }

  toggleItemSelection(item: T): void {
    if (this.itemDisabledFn(item)) {
      console.warn('Attempted to select a disabled item:', item);
      return;
    }

    const currentSelection = new Set(this.selectedItemsSource.value);
    if (currentSelection.has(item)) {
      currentSelection.delete(item);
    } else {
      currentSelection.add(item);
    }
    this.selectedItemsSource.next(currentSelection);
  }

  toggleSelectAll(): void {
    this.selectAllCheckboxState$.pipe(first()).subscribe((state) => {
      if (state.value === 'checked') {
        this.unselectAllItems();
      } else {
        this.selectAllItems();
      }
    });
  }

  selectAllItems(): void {
    this.enabledItems$.pipe(first()).subscribe((items) => {
      this.selectedItemsSource.next(new Set(items));
    });
    this.enabledItems$.pipe(first()).subscribe((items) => {
      this.selectedItemsSource.next(new Set(items));
    });
  }

  unselectAllItems(): void {
    this.selectedItemsSource.next(new Set());
  }
}
