import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MultiSelectListComponent } from './multi-select-list.component';
import { Component, Input, TemplateRef } from '@angular/core';
import { MultiSelectListService } from './services/multi-select-list.service';
import { By } from '@angular/platform-browser';
import { MockService } from '../../shared/utilities/mock-service';

@Component({ selector: 'c-multiSelectList__actions', template: '' })
class MockMultiSelectListActionsComponent {}

@Component({ selector: 'c-multiSelectList__list', template: '' })
class MockMultiSelectListListComponent {
  @Input() itemUniqueIdKey: any;
  @Input() headerTemplate: any;
  @Input() itemTemplate: any;
}

describe('MultiSelectListComponent', () => {
  let component: MultiSelectListComponent<any>;
  let fixture: ComponentFixture<MultiSelectListComponent<any>>;
  let mockService: jasmine.SpyObj<MultiSelectListService<any>>;

  beforeEach(async () => {
    mockService = MockService(MultiSelectListService);

    await TestBed.configureTestingModule({
      declarations: [
        MultiSelectListComponent,
        MockMultiSelectListActionsComponent,
        MockMultiSelectListListComponent,
      ],
    })
      .overrideComponent(MultiSelectListComponent, {
        set: {
          providers: [
            { provide: MultiSelectListService, useValue: mockService },
          ],
        },
      })
      .compileComponents();

    fixture = TestBed.createComponent(MultiSelectListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.service).toBe(mockService);
  });

  it('should set items correctly in the service when items input is set', () => {
    const testItems = [{ id: 1 }, { id: 2 }];
    component.items = testItems;
    fixture.detectChanges();
    expect(mockService.setItems).toHaveBeenCalledWith(testItems);
  });

  it('should configure itemDisabled function correctly when itemDisabledFn input is set', () => {
    const disabledFn = (item: any) => item.id === 1;
    component.itemDisabledFn = disabledFn;
    fixture.detectChanges();
    expect(component.service.itemDisabledFn).toEqual(disabledFn);
  });

  it('should pass inputs to c-multiSelectList__list component', () => {
    const testTemplateRef = {} as TemplateRef<any>;
    component.itemUniqueIdKey = 'id';
    component.headerTemplate = testTemplateRef;
    component.itemTemplate = testTemplateRef;
    fixture.detectChanges();

    const listComponent = fixture.debugElement.query(
      By.directive(MockMultiSelectListListComponent)
    ).componentInstance;
    expect(listComponent.itemUniqueIdKey).toBe('id');
    expect(listComponent.headerTemplate).toBe(testTemplateRef);
    expect(listComponent.itemTemplate).toBe(testTemplateRef);
  });
});
