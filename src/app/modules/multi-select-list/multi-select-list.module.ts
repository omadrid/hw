import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MultiSelectListComponent } from './multi-select-list.component';
import { MultiSelectListListComponent } from './components/list/list.component';
import { MultiSelectListActionsComponent } from './components/actions/actions.component';
import { MultiSelectListSelectAllCheckboxComponent } from './components/select-all-checkbox/select-all-checkbox.component';
import { FormsModule } from '@angular/forms';
import { DownloadButtonComponent } from '../../shared/components/download-button/download-button.component';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [CommonModule, FormsModule, SharedModule, DownloadButtonComponent],
  declarations: [
    MultiSelectListComponent,
    MultiSelectListActionsComponent,
    MultiSelectListListComponent,
    MultiSelectListSelectAllCheckboxComponent,
  ],
  exports: [MultiSelectListComponent],
})
export class MultiSelectListModule {}
