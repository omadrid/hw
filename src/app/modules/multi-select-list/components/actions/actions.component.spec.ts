import {
  ComponentFixture,
  TestBed,
  fakeAsync,
  tick,
  flush,
} from '@angular/core/testing';
import { MultiSelectListActionsComponent } from './actions.component';
import { MultiSelectListService } from '../../services/multi-select-list.service';
import { Component, Input } from '@angular/core';
import { MockService } from '../../../../shared/utilities/mock-service';
import { Subscription, of } from 'rxjs';
import { By } from '@angular/platform-browser';
import { SharedModule } from '../../../../shared/shared.module';

@Component({
  selector: 'c-multiSelectList__selectAllCheckbox',
  template: '<div></div>',
})
class MockSelectAllCheckboxComponent {}

@Component({
  selector: 'c-downloadButton',
  template: '<div></div>',
})
class MockDownloadButtonComponent {
  @Input() files: any;
}

describe('MultiSelectListActionsComponent', () => {
  let component: MultiSelectListActionsComponent<any>;
  let fixture: ComponentFixture<MultiSelectListActionsComponent<any>>;
  let mockMultiSelectListService: jasmine.SpyObj<MultiSelectListService<any>>;
  const testItems = new Set([
    { id: 1, name: 'Item 1' },
    { id: 2, name: 'Item 2' },
  ]);
  let subs: Subscription[] = [];

  beforeEach(async () => {
    mockMultiSelectListService = MockService(MultiSelectListService);
    mockMultiSelectListService.selectedItems$ = of(testItems);

    await TestBed.configureTestingModule({
      imports: [SharedModule],
      declarations: [
        MultiSelectListActionsComponent,
        MockSelectAllCheckboxComponent,
        MockDownloadButtonComponent,
      ],
      providers: [
        {
          provide: MultiSelectListService,
          useValue: mockMultiSelectListService,
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(MultiSelectListActionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    subs.forEach((sub) => sub.unsubscribe());
    subs = [];
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should pass correct data to downloadButton', fakeAsync(() => {
    const downloadButton = fixture.debugElement.query(
      By.directive(MockDownloadButtonComponent)
    );
    const downloadButtonInstance = downloadButton.componentInstance;

    let items;
    subs.push(
      mockMultiSelectListService.selectedItems$.subscribe(
        (result) => (items = result)
      )
    );
    tick();
    expect(items).toEqual(testItems);
    expect(downloadButtonInstance.files).toEqual(testItems);
    flush();
  }));
});
