import { Component } from '@angular/core';
import { MultiSelectListService } from '../../services/multi-select-list.service';

@Component({
  selector: 'c-multiSelectList__actions',
  templateUrl: './actions.component.html',
  styleUrls: [
    './actions.component.scss',
    '../../multi-select-list.component.scss',
  ],
})
export class MultiSelectListActionsComponent<T extends object> {
  constructor(public service: MultiSelectListService<T>) {}
}
