import { Component, Input, TemplateRef } from '@angular/core';
import { MultiSelectListService } from '../../services/multi-select-list.service';

@Component({
  selector: 'c-multiSelectList__list',
  templateUrl: './list.component.html',
  styleUrls: [
    './list.component.scss',
    '../../multi-select-list.component.scss',
  ],
})
export class MultiSelectListListComponent<T extends object> {
  /** The property used as a unique identifier for each item*/
  @Input() itemUniqueIdKey: keyof T;

  @Input() headerTemplate?: TemplateRef<any>;
  @Input() itemTemplate: TemplateRef<any>;

  constructor(public service: MultiSelectListService<T>) {}

  /**
   * Tracks each item in the list to
   * optimize rendering and minimize DOM updates
   */
  trackByItem(index: number, item: any): any {
    return item[this.itemUniqueIdKey];
  }
}
