import {
  ComponentFixture,
  TestBed,
  fakeAsync,
  flush,
  tick,
} from '@angular/core/testing';
import { MultiSelectListListComponent } from './list.component';
import {
  Component,
  Input,
  TemplateRef,
  ViewChild,
  DebugElement,
} from '@angular/core';
import { MultiSelectListService } from '../../services/multi-select-list.service';
import { By } from '@angular/platform-browser';
import { of } from 'rxjs';

@Component({
  template: `<ng-template #testTemplate let-data="data">{{
    data
  }}</ng-template>`,
})
class TestHostComponent {
  @ViewChild('testTemplate', { static: true }) templateRef!: TemplateRef<any>;
}

describe('MultiSelectListListComponent', () => {
  let component: MultiSelectListListComponent<any>;
  let fixture: ComponentFixture<MultiSelectListListComponent<any>>;
  let testHostFixture: ComponentFixture<TestHostComponent>;
  let mockService: jasmine.SpyObj<MultiSelectListService<any>>;
  let el: DebugElement;

  beforeEach(async () => {
    mockService = jasmine.createSpyObj(
      'MultiSelectListService',
      ['toggleItemSelection'],
      {
        items$: of([
          { id: 1, name: 'Item 1' },
          { id: 2, name: 'Item 2' },
        ]),
        selectedItems$: of(new Set()),
      }
    );
    mockService.itemDisabledFn = jasmine
      .createSpy('itemDisabledFn')
      .and.callFake((item: any) => false);

    await TestBed.configureTestingModule({
      declarations: [MultiSelectListListComponent, TestHostComponent],
      providers: [{ provide: MultiSelectListService, useValue: mockService }],
    }).compileComponents();

    testHostFixture = TestBed.createComponent(TestHostComponent);
    testHostFixture.detectChanges();

    fixture = TestBed.createComponent(MultiSelectListListComponent);
    component = fixture.componentInstance;
    component.itemTemplate = testHostFixture.componentInstance.templateRef;
    component.headerTemplate = testHostFixture.componentInstance.templateRef;
    component.itemUniqueIdKey = 'id';

    fixture.detectChanges();
    el = fixture.debugElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should handle checkbox click to select an item', fakeAsync(() => {
    el = fixture.debugElement;

    let checkboxes = el.queryAll(By.css('input[type="checkbox"]'));
    expect(checkboxes[0].nativeElement.checked).toBeFalse();
    expect(checkboxes[1].nativeElement.checked).toBeFalse();

    checkboxes[0].nativeElement.click();
    fixture.detectChanges();
    tick();

    checkboxes = el.queryAll(By.css('input[type="checkbox"]'));
    expect(checkboxes[0].nativeElement.checked).toBeTrue();
    expect(checkboxes[1].nativeElement.checked).toBeFalse();
    expect(mockService.toggleItemSelection).toHaveBeenCalledWith({
      id: 1,
      name: 'Item 1',
    });

    expect(mockService.toggleItemSelection.calls.count()).toEqual(1);
    flush();
  }));

  it('should handle itemDisabled logic', () => {
    component.service.itemDisabledFn = jasmine
      .createSpy('itemDisabledFn', (item: any) => item.id === 2)
      .and.callThrough();
    fixture.detectChanges();

    const checkboxes = el.queryAll(By.css('input[type="checkbox"]'));
    expect(checkboxes[1].nativeElement.disabled).toBeTrue(); // Second item is disabled
    expect(component.service.itemDisabledFn).toHaveBeenCalledWith(
      jasmine.objectContaining({ id: 2 })
    );
  });

  it('should use trackByItem for tracking items', () => {
    const trackById = component.trackByItem(0, { id: 3, name: 'Item 3' });
    expect(trackById).toBe(3);
  });

  it('should call service.toggleItemSelection when clicking a checkbox', () => {
    const checkboxes = el.queryAll(By.css('input[type="checkbox"]'));
    expect(checkboxes.length).toBe(2);

    checkboxes[0].nativeElement.click();

    expect(component.service.toggleItemSelection).toHaveBeenCalledWith(
      jasmine.objectContaining({ id: 1, name: 'Item 1' })
    );
  });
});
