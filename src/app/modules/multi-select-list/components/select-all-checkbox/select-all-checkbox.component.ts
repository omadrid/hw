import { Component } from '@angular/core';
import { MultiSelectListService } from '../../services/multi-select-list.service';
import { CheckboxState } from '../../../../shared/models/types/checkbox-state.type';
import { map } from 'rxjs';
import { AriaCheckedValue } from '../../../../shared/models/types/aria-checked-value.type';

@Component({
  selector: 'c-multiSelectList__selectAllCheckbox',
  templateUrl: './select-all-checkbox.component.html',
  styleUrls: [
    './select-all-checkbox.component.scss',
    '../../multi-select-list.component.scss',
  ],
})
export class MultiSelectListSelectAllCheckboxComponent<T extends object> {
  ariaSelectAllCheckboxValue$ = this.service.selectAllCheckboxState$.pipe(
    map((state: CheckboxState): AriaCheckedValue => {
      if (state.value === 'indeterminate') {
        return 'mixed';
      } else if (state.value === 'checked') {
        return 'true';
      } else {
        return 'false';
      }
    })
  );

  constructor(public service: MultiSelectListService<T>) {}

  onClick(): void {
    this.service.toggleSelectAll();
  }
}
