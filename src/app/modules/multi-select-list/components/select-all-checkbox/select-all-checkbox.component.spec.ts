import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MultiSelectListSelectAllCheckboxComponent } from './select-all-checkbox.component';
import { MultiSelectListService } from '../../services/multi-select-list.service';
import { BehaviorSubject, of } from 'rxjs';
import { CheckboxState } from '../../../../shared/models/types/checkbox-state.type';

describe('MultiSelectListSelectAllCheckboxComponent', () => {
  let component: MultiSelectListSelectAllCheckboxComponent<any>;
  let fixture: ComponentFixture<MultiSelectListSelectAllCheckboxComponent<any>>;
  let mockMultiSelectListService: Partial<MultiSelectListService<any>>;

  let selectAllCheckboxState$: BehaviorSubject<CheckboxState>;
  let selectedItemsCount$: BehaviorSubject<number>;
  let enabledItemsCount$: BehaviorSubject<number>;

  beforeEach(async () => {
    selectAllCheckboxState$ = new BehaviorSubject<CheckboxState>({
      value: 'unchecked',
      disabled: false,
    });
    selectedItemsCount$ = new BehaviorSubject(0);
    enabledItemsCount$ = new BehaviorSubject(10);

    mockMultiSelectListService = {
      selectAllCheckboxState$: selectAllCheckboxState$.asObservable(),
      selectedItemsCount$: selectedItemsCount$.asObservable(),
      enabledItemsCount$: enabledItemsCount$.asObservable(),
      toggleSelectAll: jasmine.createSpy('toggleSelectAll'),
    };

    await TestBed.configureTestingModule({
      declarations: [MultiSelectListSelectAllCheckboxComponent],
      providers: [
        {
          provide: MultiSelectListService,
          useValue: mockMultiSelectListService,
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(
      MultiSelectListSelectAllCheckboxComponent
    );
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  function findSelectAllCheckboxInput(): HTMLInputElement {
    return fixture.nativeElement.querySelector(
      '[data-ref="c-multiSelectList__selectAllCheckbox"]'
    );
  }

  describe('initialization', () => {
    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should initialize checkbox correctly based on the service state', () => {
      const checkbox = findSelectAllCheckboxInput();
      expect(checkbox.checked).toBeFalsy();
      expect(checkbox.disabled).toBeFalsy();
      expect(checkbox.indeterminate).toBeFalsy();
    });
  });

  describe('checkbox input states', () => {
    it('should update checkbox properties when state changes to checked', () => {
      selectAllCheckboxState$.next({ value: 'checked', disabled: false });
      fixture.detectChanges();

      const checkbox = findSelectAllCheckboxInput();
      expect(checkbox.checked).toBeTrue();
      expect(checkbox.disabled).toBeFalse();
    });

    it('should update checkbox properties when state changes to indeterminate', () => {
      selectAllCheckboxState$.next({ value: 'indeterminate', disabled: false });
      fixture.detectChanges();

      const checkbox = findSelectAllCheckboxInput();
      expect(checkbox.indeterminate).toBeTrue();
      expect(checkbox.disabled).toBeFalse();
    });

    it('should disable the checkbox when disabled property is true', () => {
      selectAllCheckboxState$.next({ value: 'unchecked', disabled: true });
      fixture.detectChanges();

      const checkbox = findSelectAllCheckboxInput();
      expect(checkbox.disabled).toBeTrue();
    });

    it('should update aria-checked based on selectAllCheckboxState$', () => {
      const checkbox = findSelectAllCheckboxInput();
      expect(checkbox.getAttribute('aria-checked')).toEqual('false');

      selectAllCheckboxState$.next({ value: 'indeterminate', disabled: false });
      fixture.detectChanges();
      expect(checkbox.getAttribute('aria-checked')).toEqual('mixed');

      selectAllCheckboxState$.next({ value: 'checked', disabled: false });
      fixture.detectChanges();
      expect(checkbox.getAttribute('aria-checked')).toEqual('true');
    });
  });

  describe('checkbox labelling', () => {
    it('should display the correct label for selected items', () => {
      const label: HTMLLabelElement = fixture.nativeElement.querySelector(
        '[data-ref="c-multiSelectList__selectAllCheckboxLabel"]'
      );
      expect(label.textContent).toContain('Selected 0');

      selectedItemsCount$.next(5);

      fixture.detectChanges();
      expect(label.textContent).toContain('Selected 5');
    });

    it('should display the correct accessibility description for select all checkbox', () => {
      const description: HTMLElement = fixture.nativeElement.querySelector(
        '[data-ref="c-multiSelectList__selectAllCheckboxDescription"]'
      );
      expect(description.textContent).toContain(
        'Toggles all available checkboxes. Currently 0 of 10 selectable items are selected.'
      );

      selectedItemsCount$.next(5);
      enabledItemsCount$.next(15);
      fixture.detectChanges();
      expect(description.textContent).toContain(
        'Toggles all available checkboxes. Currently 5 of 15 selectable items are selected.'
      );
    });
  });

  it('should call toggleSelectAll when checkbox is clicked', () => {
    const checkbox = findSelectAllCheckboxInput();
    checkbox.click();
    expect(mockMultiSelectListService.toggleSelectAll).toHaveBeenCalled();
  });
});
