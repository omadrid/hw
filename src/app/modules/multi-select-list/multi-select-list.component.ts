import { Component, Input, TemplateRef } from '@angular/core';
import { MultiSelectListService } from './services/multi-select-list.service';

/**
 * Dynamically renders a list of items,
 * each accompanied by a checkbox.
 * An action bar allows for multiselect functionality
 * and batch downloads.
 */
@Component({
  selector: 'c-multiSelectList',
  templateUrl: './multi-select-list.component.html',
  styleUrls: ['./multi-select-list.component.scss'],
  providers: [MultiSelectListService],
})
export class MultiSelectListComponent<T extends object> {
  /** All of the items to be displayed in the list */
  @Input() set items(value: T[]) {
    this.service.setItems(value);
  }

  /** Function to determine if an item is disabled */
  @Input() set itemDisabledFn(fn: (item: T) => boolean) {
    this.service.itemDisabledFn = fn;
  }

  /** The property used as a unique identifier for each item*/
  @Input() itemUniqueIdKey: string;

  /**
   * How the host component wants to display the list header row.
   * If not provided, no header will be shown.
   */
  @Input() headerTemplate?: TemplateRef<any>;

  /** How the host component wants to display each item */
  @Input() itemTemplate: TemplateRef<any>;

  constructor(public service: MultiSelectListService<T>) {}
}
