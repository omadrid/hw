import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { dummyDeviceFiles } from './dummy-data';
import { File } from '../../../../shared/models/types/file.type';

@Injectable({
  providedIn: 'root',
})
export class DeviceFilesService {
  public readonly deviceFiles$: BehaviorSubject<File[]> = new BehaviorSubject<
    File[]
  >([]);

  fetchFiles(): void {
    this.deviceFiles$.next(dummyDeviceFiles);
  }
}
