import { TestBed } from '@angular/core/testing';
import { DeviceFilesService } from './device-files.service';
import { dummyDeviceFiles } from './dummy-data';

describe('DeviceFilesService', () => {
  let service: DeviceFilesService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DeviceFilesService],
    });
    service = TestBed.inject(DeviceFilesService);
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  it('it should should fetch and populate deviceFiles$', (done: DoneFn) => {
    service.deviceFiles$.subscribe((files) => {
      if (files.length > 0) {
        expect(files).toEqual(dummyDeviceFiles);
        done();
      }
    });

    service.fetchFiles();
  });
});
