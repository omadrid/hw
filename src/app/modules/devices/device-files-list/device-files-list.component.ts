import { Component, OnInit } from '@angular/core';
import { DeviceFilesService } from '../services/device-files/device-files.service';

@Component({
  selector: 'c-deviceFilesList',
  templateUrl: './device-files-list.component.html',
  styleUrls: ['./device-files-list.component.scss'],
})
export class DeviceFilesListComponent implements OnInit {
  constructor(public service: DeviceFilesService) {}

  itemDisabledFn = (item: any): boolean => {
    return item.status !== 'available';
  };

  ngOnInit(): void {
    this.service.fetchFiles();
  }
}
