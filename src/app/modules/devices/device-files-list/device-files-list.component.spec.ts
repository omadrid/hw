import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DeviceFilesListComponent } from './device-files-list.component';
import { DeviceFilesService } from '../services/device-files/device-files.service';
import { of } from 'rxjs';
import { MockService } from '../../../shared/utilities/mock-service';
import { MultiSelectListComponent } from '../../multi-select-list/multi-select-list.component';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'c-multiSelectList',
  template: '',
})
export class MockMultiSelectListComponent {
  @Input() itemUniqueIdKey: any;
  @Input() headerTemplate: any;
  @Input() itemTemplate: any;
  @Input() items: any[];
  @Input() itemDisabledFn: Function;
}

describe('DeviceFilesListComponent', () => {
  let component: DeviceFilesListComponent;
  let fixture: ComponentFixture<DeviceFilesListComponent>;
  let mockDeviceFilesService: jasmine.SpyObj<DeviceFilesService>;

  beforeEach(async () => {
    mockDeviceFilesService = MockService(DeviceFilesService, {
      fetchFiles: jasmine.createSpy('fetchFiles'),
      deviceFiles$: of([
        {
          name: 'File1',
          device: 'Device1',
          path: '/path/to/file1',
          status: 'available',
        },
        {
          name: 'File2',
          device: 'Device2',
          path: '/path/to/file2',
          status: 'unavailable',
        },
      ]),
    });

    await TestBed.configureTestingModule({
      declarations: [DeviceFilesListComponent, MockMultiSelectListComponent],
      providers: [
        { provide: DeviceFilesService, useValue: mockDeviceFilesService },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeviceFilesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should fetch files on initialization', () => {
    expect(mockDeviceFilesService.fetchFiles).toHaveBeenCalled();
  });

  it('should disable items based on their status', () => {
    const itemAvailable = { status: 'available' };
    const itemUnavailable = { status: 'unavailable' };
    expect(component.itemDisabledFn(itemAvailable)).toBeFalse();
    expect(component.itemDisabledFn(itemUnavailable)).toBeTrue();
  });
});
