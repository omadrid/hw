import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FileStatusIndicatorComponent } from '../../shared/components/file-status-indicator/file-status-indicator.component';
import { DeviceFilesListComponent } from './device-files-list/device-files-list.component';
import { RouterModule, Routes } from '@angular/router';
import { MultiSelectListModule } from '../multi-select-list/multi-select-list.module';

const routes: Routes = [
  {
    path: 'devices',
    children: [
      {
        path: '',
        redirectTo: 'files',
        pathMatch: 'full',
      },
      {
        path: 'files',
        component: DeviceFilesListComponent,
      },
    ],
  },
];

@NgModule({
  imports: [
    CommonModule,
    FileStatusIndicatorComponent,
    RouterModule.forChild(routes),
    MultiSelectListModule,
  ],
  declarations: [DeviceFilesListComponent],
  exports: [DeviceFilesListComponent],
})
export class DevicesModule {}
