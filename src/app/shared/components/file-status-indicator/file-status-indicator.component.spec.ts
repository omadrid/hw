import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FileStatusIndicatorComponent } from './file-status-indicator.component';

describe('FileStatusIndicatorComponent', () => {
  let component: FileStatusIndicatorComponent;
  let fixture: ComponentFixture<FileStatusIndicatorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FileStatusIndicatorComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FileStatusIndicatorComponent);
    component = fixture.componentInstance;
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FileStatusIndicatorComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should not apply any dot class when status is Scheduled', () => {
    component.status = 'scheduled';
    fixture.detectChanges();
    const dotElement = fixture.nativeElement.querySelector(
      '[data-ref="c-fileStatusIndicator__dot"]'
    );
    expect(dotElement.classList.contains('c-fileStatusIndicator__dot--')).toBe(
      false
    );
  });

  it('should apply green dot class when status is Available', () => {
    component.status = 'available';
    fixture.detectChanges();
    const dotElement = fixture.nativeElement.querySelector(
      '[data-ref="c-fileStatusIndicator__dot"]'
    );
    expect(
      dotElement.classList.contains('c-fileStatusIndicator__dot--green')
    ).toBe(true);
  });

  it('should display "Available" status when status is Available', () => {
    component.status = 'available';
    fixture.detectChanges();
    const statusElement = fixture.nativeElement.querySelector(
      '[data-ref="c-fileStatusIndicator__name"]'
    );
    expect(statusElement.textContent.trim()).toBe('Available');
  });

  it('should display "Scheduled" status when status is Scheduled', () => {
    component.status = 'scheduled';
    fixture.detectChanges();
    const statusElement = fixture.nativeElement.querySelector(
      '[data-ref="c-fileStatusIndicator__name"]'
    );
    expect(statusElement.textContent.trim()).toBe('Scheduled');
  });

  it('should not render anything when no status is provided', () => {
    fixture.detectChanges();
    const dotElement = fixture.nativeElement.querySelector(
      '[data-ref="c-fileStatusIndicator__dot"]'
    );
    const statusElement = fixture.nativeElement.querySelector(
      '[data-ref="c-fileStatusIndicator__name"]'
    );
    expect(dotElement).toBeNull();
    expect(statusElement).toBeNull();
  });
});
