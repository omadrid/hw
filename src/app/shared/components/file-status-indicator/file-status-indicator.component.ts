import { Component, Input, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FileStatus } from '../../models/types/file-status.type';

type FileStatusIndicatorDotColor = 'green';

type FileStatusIndicatorConfig = {
  status: FileStatus;
  dotColor: FileStatusIndicatorDotColor | null;
};

const FILE_STATUS_INDICATOR_CONFIG: FileStatusIndicatorConfig[] = [
  { status: 'available', dotColor: 'green' },
  { status: 'scheduled', dotColor: null },
];

@Component({
  selector: 'c-fileStatusIndicator',
  imports: [CommonModule],
  templateUrl: './file-status-indicator.component.html',
  styleUrls: ['./file-status-indicator.component.scss'],
  standalone: true,
})
export class FileStatusIndicatorComponent implements OnInit {
  @Input() status!: FileStatus;

  dotColor: FileStatusIndicatorDotColor | null = null;

  ngOnInit(): void {
    this.dotColor = this.getDotColor();
  }

  getDotColor(): FileStatusIndicatorDotColor | null {
    const config: FileStatusIndicatorConfig | undefined =
      FILE_STATUS_INDICATOR_CONFIG.find((item) => item.status === this.status);

    return config?.dotColor ?? null;
  }
}
