import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CommonModule } from '@angular/common';
import { DownloadButtonComponent } from './download-button.component';
import { File } from '../../models/types/file.type';

describe('DownloadButtonComponent', () => {
  let component: DownloadButtonComponent;
  let fixture: ComponentFixture<DownloadButtonComponent>;
  let buttonEl: HTMLButtonElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CommonModule, DownloadButtonComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DownloadButtonComponent);
    component = fixture.componentInstance;
    buttonEl = fixture.debugElement.nativeElement.querySelector('button');
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('when there are no files provided', () => {
    it('should be disabled', () => {
      expect(buttonEl.disabled).toBeTrue();
      expect(
        buttonEl.classList.contains('c-downloadButton--disabled')
      ).toBeTrue();
    });
  });

  describe('when there are files provided', () => {
    beforeEach(() => {
      const mockFiles = new Set<File>([
        { path: 'file1.txt', name: 'File 1', status: 'available' },
        {
          path: 'file2.txt',
          name: 'File 2',
          status: 'available',
          device: 'Device 1',
        },
      ]);
      component.files = mockFiles;
      fixture.detectChanges();
    });

    it('should enable button and remove disabled class when files are present', () => {
      expect(buttonEl.disabled).toBeFalse();
      expect(
        buttonEl.classList.contains('c-downloadButton--disabled')
      ).toBeFalse();
    });

    it('should call window.alert when clicked', () => {
      spyOn(window, 'alert');
      buttonEl.click();
      expect(window.alert).toHaveBeenCalled();
    });

    it('should generate correct alert text when clicked', () => {
      const expectedAlertText =
        'Preparing to download:\nfile1.txt\nfile2.txt (Device 1)';
      spyOn(window, 'alert');
      buttonEl.click();
      expect(window.alert).toHaveBeenCalledWith(expectedAlertText);
    });
  });
});
