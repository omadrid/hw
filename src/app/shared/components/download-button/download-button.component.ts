import { Component, Input } from '@angular/core';
import { File } from '../../models/types/file.type';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'c-downloadButton',
  templateUrl: './download-button.component.html',
  styleUrls: ['./download-button.component.scss'],
  imports: [CommonModule],
  standalone: true,
})
export class DownloadButtonComponent {
  @Input() files: Set<File> = new Set();

  onClick(): void {
    if (this.files.size === 0) return;

    let alertText = 'Preparing to download:';
    alertText += Array.from(this.files)
      .map((file) => this.getAlertTextForFile(file))
      .join('');

    window.alert(alertText);
  }

  getAlertTextForFile(file: File): string {
    const deviceText = file.device ? ` (${file.device})` : '';
    return `\n${file.path}${deviceText}`;
  }
}
