import { FileStatus } from './file-status.type';

export type File = {
  path: string;
  name: string;
  status: FileStatus;
  device?: string;
};
