export type AriaCheckedValue = 'true' | 'false' | 'mixed';
