export type CheckboxValue = 'checked' | 'unchecked' | 'indeterminate';

export type CheckboxState = {
  value: CheckboxValue;
  disabled: boolean;
};
