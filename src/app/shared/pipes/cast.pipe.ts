import { Pipe, PipeTransform } from '@angular/core';

/** Performs type conversions */
@Pipe({
  name: 'cast',
})
export class CastPipe implements PipeTransform {
  transform<T>(value: any, targetType = ''): T {
    if (targetType === 'number' && typeof value === 'string') {
      return Number(value) as unknown as T;
    }
    return value as T;
  }
}
