import { CastPipe } from './cast.pipe';

describe('CastPipe', () => {
  let pipe: CastPipe;

  beforeEach(() => {
    pipe = new CastPipe();
  });

  it('should create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should convert a string to a number when asked', () => {
    const num = pipe.transform<number>('123', 'number');
    expect(num).toBe(123);
    expect(typeof num).toBe('number');
  });

  it('should handle null and undefined inputs gracefully', () => {
    const resultNull = pipe.transform<null>(null);
    const resultUndefined = pipe.transform<undefined>(undefined);
    expect(resultNull).toBeNull();
    expect(resultUndefined).toBeUndefined();
  });
});
