import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CastPipe } from './pipes/cast.pipe';

const SHARED_PIPES = [CastPipe];

@NgModule({
  declarations: [...SHARED_PIPES],
  imports: [CommonModule],
  exports: [...SHARED_PIPES],
})
export class SharedModule {}
