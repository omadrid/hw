/**
 * Dynamically generate a mock object with spies
 * on all the prototype methods of a given service class
 * and allows optional overrides for properties and method behaviors
 *
 * Credit: https://gitlab.com/minds/front/-/blob/master/src/app/utils/mock.ts
 * */
export function MockService(
  serviceClass: any,
  overrides?: Record<string, any>
) {
  const service = jasmine.createSpyObj(
    serviceClass.name,
    Object.getOwnPropertyNames(serviceClass.prototype)
  );

  if (overrides) {
    const hasOwnProperty = Object.prototype.hasOwnProperty;
    for (const key in overrides) {
      if (hasOwnProperty.call(overrides, key)) {
        service[key] = overrides[key];
      }
    }
  }

  return service;
}
